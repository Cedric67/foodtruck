﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.DAO
{
    public class DataBase
    {
        internal static SqlConnection Connection;

        public void OpenConnection(string connectionStrings)
        {
            Connection = new SqlConnection(connectionStrings);
            Connection.Open();
        }

        public void CloseConnection()
        {
            Connection.Close();
        }
    }
}
