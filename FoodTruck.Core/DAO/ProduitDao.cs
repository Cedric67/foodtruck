﻿using FoodTruck.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.DAO
{
    public class ProduitDao : BaseDao
    {
        private Produit Produit;
        
        protected override object GetValues(SqlDataReader reader)
        {
            return new Produit
                ((int)reader["ID_PRODUIT"],
                (int)reader["ID_FAMILLE_REPAS"],
                reader["LIBELLE_PRODUIT"].ToString(),
                reader["DESCRIPTION"].ToString(),
                (int)reader["NOMBRE_DE_VENTE"],
                (decimal)reader["PRIX"],
                reader["URL_IMAGE"].ToString(),
                (int)reader["STOCK"],
                reader["LMMJVSD"].ToString(),
                (double)reader["MOYENNE_NOTE"],
                reader["UNITE"].ToString());
        }

        public override bool Insert(object obj)
        {
            Produit = Setter<Produit>(obj);
            if (Produit == null) return false;

            Command.CommandText = "INSERT INTO PRODUIT VALUES (@IdFamilleRepas, @LibelleProduit, " +
                                "@DescriptionProduit, @NbVenteProduit, @PrixProduit, @UrlImageProduit, " +
                                "@StockProduit, @LMMJVSD, @MoyNoteProduit, @UniteProduit)";
            SetParameters();

            return Execute();
        }

        public override bool UpdateById(object obj, int id)
        {
            Produit = Setter<Produit>(obj);
            if (Produit == null) return false;

            Command.CommandText = "UPDATE PRODUIT SET " +
                                "ID_FAMILLE_REPAS = @IdFamilleRepas, " +
                                "LIBELLE_PRODUIT = @LibelleProduit, " +
                                "DESCRIPTION = @DescriptionProduit, " +
                                "NOMBRE_DE_VENTE = @NbVenteProduit, " +
                                "PRIX = @PrixProduit, " +
                                "URL_IMAGE = @UrlImageProduit, " +
                                "STOCK = @StockProduit, " +
                                "LMMJVSD = @LMMJVSD, " +
                                "MOYENNE_NOTE = @MoyNoteProduit, " +
                                "UNITE = @UniteProduit " +
                                "WHERE ID_PRODUIT = @IdProduit";
            SetParameters();

            return Execute();
        }

        protected override void SetParameters()
        {
            Command.Parameters.AddWithValue("@IdProduit", Produit.Id_Produit);
            Command.Parameters.AddWithValue("@IdFamilleRepas", Produit.Id_Famille_Repas);
            Command.Parameters.AddWithValue("@LibelleProduit", Produit.Libelle_Produit);
            Command.Parameters.AddWithValue("@DescriptionProduit", Produit.Description);
            Command.Parameters.AddWithValue("@NbVenteProduit", Produit.Nombre_De_Vente);
            Command.Parameters.AddWithValue("@PrixProduit", Produit.Prix);
            Command.Parameters.AddWithValue("@UrlImageProduit", Produit.Url_Image);
            Command.Parameters.AddWithValue("@StockProduit", Produit.Stock);
            Command.Parameters.AddWithValue("@LMMJVSD", Produit.LMMJVSD);
            Command.Parameters.AddWithValue("@MoyNoteProduit", Produit.Moyenne_Note);
            Command.Parameters.AddWithValue("@UniteProduit", Produit.Unite);
        }
    }
}
