﻿using FoodTruck.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.DAO
{
    public class ActualitesDao : BaseDao
    {
        private Actualites Actualite;
        protected override object GetValues(SqlDataReader reader)
        {
            return new Actualites
                ((int)reader["ID_ACTUALITES"],
                reader["TITRE"].ToString(),
                reader["DESCRIPTION"].ToString(),
                reader["URL_IMAGE"].ToString(),
                (DateTime)reader["DATE_DEBUT"],
                (DateTime)reader["DATE_FIN"],
                reader["LINK"].ToString(),
                (int)reader["ID_UTILISATEUR"],
                (bool)reader["ACTIF"]);
        }

        public override bool Insert(object obj)
        {
            Actualite = Setter<Actualites>(obj);
            if (Actualite == null) return false;
            
            Command.CommandText = "INSERT INTO ACTUALITES (ID_UTILISATEUR, LINK, TITRE, URL_IMAGE, ACTIF, DATE_DEBUT, DATE_FIN, DESCRIPTION)" +
                "VALUES(@Lastname, @Firstname, @Birthdate, @Weight, @Grade)";
            SetParameters();

            return Execute();
        }

        public override bool UpdateById(object obj, int id)
        {
            Actualite = Setter<Actualites>(obj);
            if (Actualite == null) return false;

            Command.CommandText = "UPDATE ACTUALITE SET " +
                                "ID_UTILISATEUR = @Id_Utilisateur, " +
                                "LINK = @Link, " +
                                "TITRE = @Titre, " +
                                "URL_IMAGE = @Url_Image, " +
                                "ACTIF = @Actif, " +
                                "URL_IMAGE = @UrlImageProduit, " +
                                "DATE_DEBUT = @Date_Debut, " +
                                "DATE_FIN = @Date_Fin, " +
                                "DESCRIPTION = @Description, " +
                                "WHERE ID_ACTUALITE = @Id_Actualite";
            SetParameters();

            return Execute();
        }

        protected override void SetParameters()
        {
            Command.Parameters.AddWithValue("@Id_Actualite", Actualite.Id_Actualite);
            Command.Parameters.AddWithValue("@Id_Utilisateur", Actualite.Id_Utilisateur);
            Command.Parameters.AddWithValue("@Link", Actualite.Link);
            Command.Parameters.AddWithValue("@Titre", Actualite.Titre);
            Command.Parameters.AddWithValue("@Url_Image", Actualite.Url_Image);
            Command.Parameters.AddWithValue("@Actif", Actualite.Actif);
            Command.Parameters.AddWithValue("@Date_Debut", Actualite.Date_Debut);
            Command.Parameters.AddWithValue("@Date_Fin", Actualite.Date_Fin);
            Command.Parameters.AddWithValue("@Description", Actualite.Description);
        }
    }
}
