﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.DAO
{
    public abstract class BaseDao
    {
        protected static SqlCommand Command = DataBase.Connection.CreateCommand();

        protected object FindById(Type clazz, int id)
        {
            object objetct = Activator.CreateInstance(clazz);

            Command.CommandText = "Select * from" +
                clazz.ToString() +
                "where ID = @Id";
            Command.Parameters.AddWithValue("@Id", id);

            using (SqlDataReader reader = Command.ExecuteReader())
            {
                while (reader.Read())
                {
                    objetct = GetValues(reader);
                }
            }

            return objetct;
        }

        protected List<object> FindAll(Type clazz)
        {
            var objects = new List<object>();

            Command.CommandText = "Select * from" + clazz.ToString();

            using (SqlDataReader reader = Command.ExecuteReader())
            {
                while (reader.Read())
                {
                    objects.Add(GetValues(reader));
                }
            }

            return objects;
        }

        protected abstract object GetValues(SqlDataReader reader);

        public abstract bool Insert(object obj);

        public abstract bool UpdateById(object obj, int id);

        protected abstract void SetParameters();

        protected bool DeleteById(Type clazz, int id)
        {
            Command.CommandText = "DELETE FROM" +
                clazz.ToString() +
                "WHERE ID_PRODUIT = @id";
            Command.Parameters.AddWithValue("@id", id);
            if (Command.ExecuteNonQuery() == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected bool Execute()
        {
            if (Command.ExecuteNonQuery() == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected T Setter<T>(object obj)
        {
            if(obj.GetType() == typeof(T))
            {
                return (T)obj;
            }
            else
            {
                return default(T);
            }
        }
    }
}
