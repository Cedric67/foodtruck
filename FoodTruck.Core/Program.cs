﻿using FoodTruck.Core.DAO;
using FoodTruck.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core
{
    class Program
    {
        static void Main(string[] args)
        {
            //Prérequis au lancement du programme
            var connexion = new DataBase();
            connexion.OpenConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            // Lancement du programme
            launch();

            // Fermeture du programme
            connexion.CloseConnection();
        }

        static void launch()
        {
            //Execution du programme
            ActualitesDao actualitesDao = new ActualitesDao();
            actualitesDao.FindAll(typeof(Actualites));
        }
    }
}
