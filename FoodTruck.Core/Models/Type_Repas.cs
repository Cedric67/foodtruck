﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Type_Repas : BindableBase
    {
        public Type_Repas()
        {
        }

        public Type_Repas(int id_Type_Repas, string libelle_Repas)
        {
            Id_Type_Repas = id_Type_Repas;
            Libelle_Repas = libelle_Repas;
        }

        public Type_Repas(string libelle_Repas)
        {
            Libelle_Repas = libelle_Repas;
        }

        private int id_type_repas;

        public int Id_Type_Repas
        {
            get => id_type_repas;
            set => SetProperty(ref id_type_repas, value);
        }

        private string libelle_repas;

        public string Libelle_Repas
        {
            get => libelle_repas;
            set => SetProperty(ref libelle_repas, value);
        }

    }
}
