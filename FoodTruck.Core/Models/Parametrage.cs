﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Parametrage : BindableBase
    {
        public Parametrage()
        {
        }

        public Parametrage(int id_Parametrage, string plage, decimal prix)
        {
            Id_Parametrage = id_Parametrage;
            Plage = plage;
            Prix = prix;
        }

        public Parametrage(string plage, decimal prix)
        {
            Plage = plage;
            Prix = prix;
        }

        private int id_parametrage;

        public int Id_Parametrage
        {
            get => id_parametrage;
            set => SetProperty(ref id_parametrage, value);
        }

        private string plage;

        public string Plage
        {
            get => plage;
            set => SetProperty(ref plage, value);
        }

        private decimal prix;

        public decimal Prix
        {
            get => prix;
            set => SetProperty(ref prix, value);
        }

    }
}
