﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Genre : BindableBase
    {
        public Genre()
        {
        }

        public Genre(int id_Genre, string libelle_Genre)
        {
            Id_Genre = id_Genre;
            Libelle_Genre = libelle_Genre;
        }

        public Genre(string libelle_Genre)
        {
            Libelle_Genre = libelle_Genre;
        }

        private int id_genre;

        public int Id_Genre
        {
            get => id_genre;
            set => SetProperty(ref id_genre, value);
        }

        private string libelle_genre;

        public string Libelle_Genre
        {
            get => libelle_genre;
            set => SetProperty(ref libelle_genre, value);
        }

    }
}
