﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Profil : BindableBase
    {
        public Profil()
        {
        }

        public Profil(int id_Profil, string libelle_Profile)
        {
            Id_Profil = id_Profil;
            Libelle_Profile = libelle_Profile;
        }

        public Profil(string libelle_Profile)
        {
            Libelle_Profile = libelle_Profile;
        }

        private int id_profil;

        public int Id_Profil
        {
            get => id_profil;
            set => SetProperty(ref id_profil, value);
        }

        private string libelle_profil;

        public string Libelle_Profile
        {
            get => libelle_profil;
            set => SetProperty(ref libelle_profil, value);
        }

    }
}
