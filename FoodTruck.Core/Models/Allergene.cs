﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Allergene : BindableBase
    {
        public Allergene()
        {
        }

        public Allergene(int id_Allergene, string libelle_Allergene, string code_Allergene)
        {
            Id_Allergene = id_Allergene;
            Libelle_Allergene = libelle_Allergene;
            Code_Allergene = code_Allergene;
        }

        public Allergene(string libelle_Allergene, string code_Allergene)
        {
            Libelle_Allergene = libelle_Allergene;
            Code_Allergene = code_Allergene;
        }

        private int id_allergene;

        public int Id_Allergene
        {
            get => id_allergene;
            set => SetProperty(ref id_allergene, value);
        }


        private string libelle_allergene    ;

        public string Libelle_Allergene
        {
            get => libelle_allergene;
            set => SetProperty(ref libelle_allergene, value);
        }


        private string code_allergene;

        public string Code_Allergene
        {
            get => code_allergene;
            set => SetProperty(ref code_allergene, value);
        }
    }
}
