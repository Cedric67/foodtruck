﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Type_Famille_Repas : BindableBase
    {
        public Type_Famille_Repas()
        {
        }

        public Type_Famille_Repas(int id_Type_Repas, int id_Famille_Repas) : this(id_Type_Repas)
        {
            Id_Famille_Repas = id_Famille_Repas;
        }

        public Type_Famille_Repas(int id_Famille_Repas)
        {
            Id_Famille_Repas = id_Famille_Repas;
        }

        private int id_type_repas;

        public int Id_Type_Repas
        {
            get => id_type_repas;
            set => SetProperty(ref id_type_repas, value);
        }

        private int id_famille_repas;

        public int Id_Famille_Repas
        {
            get => id_famille_repas;
            set => SetProperty(ref id_famille_repas, value);
        }

    }
}
