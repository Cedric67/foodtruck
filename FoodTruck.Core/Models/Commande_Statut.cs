﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Commande_Statut : BindableBase
    {
        public Commande_Statut(int libelle_Commande_Status)
        {
            Libelle_Commande_Status = libelle_Commande_Status;
        }

        public Commande_Statut(int id_Commande_Status, int libelle_Commande_Status) : this(id_Commande_Status)
        {
            Libelle_Commande_Status = libelle_Commande_Status;
        }

        public Commande_Statut()
        {
        }

        private int id_commande_status;

        public int Id_Commande_Status
        {
            get => id_commande_status;
            set => SetProperty(ref id_commande_status, value);
        }


        private int libelle_commande_status;

        public int Libelle_Commande_Status
        {
            get => libelle_commande_status;
            set => SetProperty(ref libelle_commande_status, value);
        }
    }
}
