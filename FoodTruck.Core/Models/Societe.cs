﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Societe : BindableBase
    {
        public Societe()
        {
        }

        public Societe(int id_Societe, string libelle_Societe, string numero_Rue, string rue, string code_Postale, string ville, string pays)
        {
            Id_Societe = id_Societe;
            Libelle_Societe = libelle_Societe;
            Numero_Rue = numero_Rue;
            Rue = rue;
            Code_Postale = code_Postale;
            Ville = ville;
            Pays = pays;
        }

        public Societe(string libelle_Societe, string numero_Rue, string rue, string code_Postale, string ville, string pays)
        {
            Libelle_Societe = libelle_Societe;
            Numero_Rue = numero_Rue;
            Rue = rue;
            Code_Postale = code_Postale;
            Ville = ville;
            Pays = pays;
        }

        private int id_societe;

        public int Id_Societe
        {
            get => id_societe;
            set => SetProperty(ref id_societe, value);
        }

        private string libelle_societe;

        public string Libelle_Societe
        {
            get => libelle_societe;
            set => SetProperty(ref libelle_societe, value);
        }

        private string numero_rue;

        public string Numero_Rue
        {
            get => numero_rue;
            set => SetProperty(ref numero_rue, value);
        }

        private string rue;

        public string Rue
        {
            get => rue;
            set => SetProperty(ref rue, value);
        }

        private string code_postale;

        public string Code_Postale
        {
            get => code_postale;
            set => SetProperty(ref code_postale, value);
        }

        private string ville;

        public string Ville
        {
            get => ville;
            set => SetProperty(ref ville, value);
        }

        private string pays;

        public string Pays
        {
            get => pays;
            set => SetProperty(ref pays, value);
        }

    }
}
