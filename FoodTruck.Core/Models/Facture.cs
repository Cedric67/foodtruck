﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Facture : BindableBase
    {
        public Facture(int id_Commande, int id_Adresse)
        {
            Id_Commande = id_Commande;
            Id_Adresse = id_Adresse;
        }

        public Facture(int id_Facture, int id_Commande, int id_Adresse) : this(id_Facture, id_Commande)
        {
            Id_Adresse = id_Adresse;
        }

        public Facture()
        {
        }

        private int id_facture;

        public int Id_Facture
        {
            get => id_facture;
            set => SetProperty(ref id_facture, value);
        }

        private int id_commande;

        public int Id_Commande
        {
            get => id_commande;
            set => SetProperty(ref id_commande, value);
        }

        private int id_adresse;

        public int Id_Adresse
        {
            get => id_adresse;
            set => SetProperty(ref id_adresse, value);
        }
    }
}
