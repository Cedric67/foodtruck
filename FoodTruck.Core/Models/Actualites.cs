﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Actualites : BindableBase
    {
        public Actualites()
        {
        }

        public Actualites(int id_Actualite, string titre, string description, string url_Image, DateTime date_Debut, DateTime date_Fin, string link, int id_Utilisateur, bool actif)
        {
            Id_Actualite = id_Actualite;
            Titre = titre;
            Description = description;
            Url_Image = url_Image;
            Date_Debut = date_Debut;
            Date_Fin = date_Fin;
            Link = link;
            Id_Utilisateur = id_Utilisateur;
            Actif = actif;
        }

        public Actualites(string titre, string description, string url_Image, DateTime date_Debut, DateTime date_Fin, string link, int id_Utilisateur, bool actif)
        {
            Titre = titre;
            Description = description;
            Url_Image = url_Image;
            Date_Debut = date_Debut;
            Date_Fin = date_Fin;
            Link = link;
            Id_Utilisateur = id_Utilisateur;
            Actif = actif;
        }

        private int id_actualite;

        public int Id_Actualite { get => id_actualite; set => SetProperty(ref id_actualite, value); }

        private string titre;

        public string Titre { get => titre; set => SetProperty(ref titre, value); }

        private string description;

        public string Description { get => description; set => SetProperty(ref description, value); }

        private string url_image;

        public string Url_Image { get => url_image; set => SetProperty(ref url_image, value); }

        private DateTime date_debut;

        public DateTime Date_Debut { get => date_debut; set => SetProperty(ref date_debut, value); }

        private DateTime date_fin;

        public DateTime Date_Fin { get => date_fin; set => SetProperty(ref date_fin, value); }

        private string link;

        public string Link { get => link; set => SetProperty(ref link, value); }

        private int id_utilisateur;

        public int Id_Utilisateur { get => id_utilisateur; set => SetProperty(ref id_utilisateur, value); }

        private bool actif;

        public bool Actif { get => actif; set => SetProperty(ref actif, value); }

    }
}
