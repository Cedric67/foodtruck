﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Famille_Repas : BindableBase
    {
        public Famille_Repas(string libelle_Famille_Repas)
        {
            Libelle_Famille_Repas = libelle_Famille_Repas;
        }

        public Famille_Repas(int id_Famille_Repas, string libelle_Famille_Repas)
        {
            Id_Famille_Repas = id_Famille_Repas;
            Libelle_Famille_Repas = libelle_Famille_Repas;
        }

        public Famille_Repas()
        {
        }

        private int id_famille_repas;

        public int Id_Famille_Repas
        {
            get => id_famille_repas;
            set => SetProperty(ref id_famille_repas, value);
        }

        private string libelle_famille_repas;


        public string Libelle_Famille_Repas
        {
            get => libelle_famille_repas;
            set => SetProperty(ref libelle_famille_repas, value);
        }

    }
}
