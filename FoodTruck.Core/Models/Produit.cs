﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.Core.Models
{
    public class Produit : BindableBase
    {
        public Produit()
        {
        }

        public Produit(int id_Produit, int id_Famille_Repas, string libelle_Produit, string description, int nombre_De_Vente, decimal prix, string url_Image, int stock, string lMMJVSD, double moyenne_Note, string unite)
        {
            Id_Produit = id_Produit;
            Id_Famille_Repas = id_Famille_Repas;
            Libelle_Produit = libelle_Produit;
            Description = description;
            Nombre_De_Vente = nombre_De_Vente;
            Prix = prix;
            Url_Image = url_Image;
            Stock = stock;
            LMMJVSD = lMMJVSD;
            Moyenne_Note = moyenne_Note;
            Unite = unite;
        }

        public Produit(int id_Famille_Repas, string libelle_Produit, string description, int nombre_De_Vente, decimal prix, string url_Image, int stock, string lMMJVSD, double moyenne_Note, string unite)
        {
            Id_Famille_Repas = id_Famille_Repas;
            Libelle_Produit = libelle_Produit;
            Description = description;
            Nombre_De_Vente = nombre_De_Vente;
            Prix = prix;
            Url_Image = url_Image;
            Stock = stock;
            LMMJVSD = lMMJVSD;
            Moyenne_Note = moyenne_Note;
            Unite = unite;
        }

        private int id_produit;

        public int Id_Produit
        {
            get => id_produit;
            set => SetProperty(ref id_produit, value);
        }

        private int id_famille_repas;

        public int Id_Famille_Repas
        {
            get => id_famille_repas;
            set => SetProperty(ref id_famille_repas, value);
        }

        private string libelle_produit;

        public string Libelle_Produit
        {
            get => libelle_produit;
            set => SetProperty(ref libelle_produit, value);
        }

        private string description;

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }

        private int nombre_de_vente;

        public int Nombre_De_Vente
        {
            get => nombre_de_vente;
            set => SetProperty(ref nombre_de_vente, value);
        }

        private decimal prix;

        public decimal Prix
        {
            get => prix;
            set => SetProperty(ref prix, value);
        }

        private string url_image;

        public string Url_Image
        {
            get => url_image;
            set => SetProperty(ref url_image, value);
        }

        private int stock;

        public int Stock
        {
            get => stock;
            set => SetProperty(ref stock, value);
        }

        private string lmmjvsd;

        public string LMMJVSD
        {
            get => lmmjvsd;
            set => SetProperty(ref lmmjvsd, value);
        }

        private double moyenne_note;

        public double Moyenne_Note
        {
            get => moyenne_note;
            set => SetProperty(ref moyenne_note, value);
        }

        private string unite;

        public string Unite
        {
            get => unite;
            set => SetProperty(ref unite, value);
        }

    }
}
